######################################################################
#Package imports

import os
from PIL import Image
from PIL import ImageFilter
import numpy as np
from sklearn.linear_model import SGDClassifier
from sklearn import cross_validation
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.svm import SVC
from sklearn.decomposition import RandomizedPCA
from pybrain.datasets import ClassificationDataSet, SupervisedDataSet
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SoftmaxLayer
from sklearn.cluster import MiniBatchKMeans
import scipy
from scipy.sparse import hstack, vstack, coo_matrix, csr_matrix, csc_matrix
import csv

######################################################################
#Directory variable declarations

DIRECTORY = 'C:\Users\leland.lockhart\Desktop\cats-and-dogs-kaggle\\'

TRAIN = 'data/train/train'

TEST = 'data/test1/test1'

######################################################################
#Function definitions

def get_imlist(path):
	"""Returns a list of filenames
	for all jpegs in a directory"""

	return[os.path.join(path,f) for f in os.listdir(path) if f.endswith('.jpg')]


def to_array(image, grayscale=False):
	"""Converts an image into an array
	of grayscale pixels."""

	if grayscale:
		return array(Image.open(image).convert('L'))
	else:
		return array(Image.open(image))

def imresize(im,sz):
	"""Resize an image array using PIL."""

	pil_im = Image.fromarray(uint8(im))
	return array(pil_im.resize(sz))

def plot_image(image, grayscale = False):
	"""Plot an image in pylab."""

	if grayscale:
		imshow(array(Image.open(image).convert('L')))
	else:
		imshow(array(Image.open(image)))

def create_vector(image, size):
	"""Resize image and create a 1-D grayscale vector for input 
	to machine learning models."""

	temp = imresize(to_array(image,grayscale = True),size).flatten()
	return temp.reshape((1,temp.shape[0]))

def normalize_vector(vector):
	"""Normalize a vector."""
	return vector/np.sum(np.abs(vector)**2,axis=-1)**(1./2)

def create_feature_array(images):
	"""Creates an array of SIFT features for all pictures
	to be used as input matrix for modeling.
	Returns normalized descriptions."""
	locations = np.array([])
	descriptions = np.array([])
	for i, im in enumerate(images):
		if i  % 100 == 0:
			print "Processing image %i." % i
		sift_transformation(im, 'test.txt')
		locs, descs = read_sift_file('test.txt')
		descs = coo_matrix(apply_along_axis(normalize_vector,1,descs))
		if locations.any():
			locations = np.vstack((locations,locs))
			descriptions = scipy.sparse.vstack((descriptions, descs))
		else:
			locations = locs
			descriptions = coo_matrix(descs)
		os.remove('test.txt')

	return locations, descriptions

def batch_kmeans(images, k=1000):
	"""Run kmeans clustering in mini-batches over
	all images.  Reduces memory footprint by not holding
	all vectors in memory at once"""

	kmeans = MiniBatchKMeans(n_clusters=k, init='k-means++', 
		max_iter=100, batch_size=100, verbose=0, 
		compute_labels=True, random_state=57883, tol=0.0, max_no_improvement=10, 
		init_size=None, n_init=3, reassignment_ratio=0.01)

	locations = np.array([])
	descriptions = np.array([])
	for i,im in enumerate(images):
		sift_transformation(im, 'test.txt')
		locs, descs = read_sift_file('test.txt')
		if locations.any():
			locations = np.vstack((locations,locs))
			descriptions = scipy.sparse.vstack((descriptions, descs))
		else:
			locations = locs
			descriptions = coo_matrix(descs)

		if i % 1000 == 0:
			print "Processing record: %i." % i
			kmeans.partial_fit(descriptions)
			locations = np.array([])
			descriptions = np.array([])

	return kmeans

def get_labels(directory):
	"""Creates vector of labels from filenames"""

	return_to = os.getcwd()
	os.chdir(directory)
	labels = []
	for im in get_imlist(os.getcwd()):
		labels.append(im.split('.')[1][-3:])
	os.chdir(return_to)

	vector = np.array([1 if x == 'dog' else 0 for x in labels])

	return vector

def sift_transformation(imagename, resultname, params = "--edge-thresh 10 --peak-thresh 5"):
	"""Create SIFT transformation of an image."""

	if imagename[-3:] != 'pgm':
		#create pgm file
		im = Image.open(imagename).convert('L')
		im.save('tmp.pgm')
		imagename = 'tmp.pgm'

	cmmd = str("sift "+imagename+" --output="+resultname+" "+params)
	os.system(cmmd)
	os.remove('tmp.pgm')

def read_sift_file(filename):
	"""Import a SIFT transformed image text file."""

	f = np.loadtxt(filename)
	return f[:,:4], f[:,4:] #feature locations, descriptions

def plot_features(im, locs, circle=False):
	"""Show image with features.
	Input:  im (image as array)
	locs(row, col, scale, orientation of each feature)
	"""

	def draw_circle(c,r):
		t = np.arange(0,1.01, .01) * 2 * pi
		x = r* cos(t) + c[0]
		y = r* sin(t) + c[1]
		plot(x,y,'b',linewidth=2)

	imshow(im)
	if circle:
		for p in locs:
			draw_circle(p[:2], p[2])
	else:
		plot(locs[:,0], locs[:1], 'ob')
	axis('off')


def sample_sift_plot(image):
	im1 = to_array(image)
	sift_transformation(image,'test.txt')
	l1,d1 = read_sift_file('test.txt')

	figure()
	gray()
	plot_features(im1,l1,circle=True)
	show()

######################################################################
#Processing

#put all images in a list
images = get_imlist(DIRECTORY + TRAIN)

clustering = batch_kmeans(images)

_ , descriptions = create_feature_array(images)

labels = get_labels(DIRECTORY + TRAIN)
















ds = ClassificationDataSet(400, nb_classes = 2, class_labels = ['cat','dog'])
for i in range(vectors.shape[0]):
	ds.addSample(vectors[i,:],[labels[i]])

tstdata, trndata = ds.splitWithProportion(.25)
trndata._convertToOneOfMany()
tstdata._convertToOneOfMany()


nnet = buildNetwork(trndata.indim, 20, 20, trndata.outdim, 
	bias = True, outclass = SoftmaxLayer)

trainer = BackpropTrainer(nnet, dataset=trndata, momentum = 0.99, verbose = True,
	weightdecay = 1.0, learningrate = 1.0, lrdecay=1.0)

for i in range (5):

	trainer.trainEpochs(1)
	trnresult = percentError( trainer.testOnClassData(dataset=trndata), trndata['class'])
	tstresult = percentError( trainer.testOnClassData(dataset=tstdata), tstdata['class'])
	print "Epoch: %4d  Train Error: %5.2f%%  Test Error: %5.2f%%" % (trainer.totalepochs,
		trnresult, tstresult)

trainer.trainUntilConvergence()
trnresult = percentError( trainer.testOnClassData(dataset=trndata), trndata['class'])
tstresult = percentError( trainer.testOnClassData(dataset=tstdata), tstdata['class'])
print "Epoch: %4d  Train Error: %5.2f%%  Test Error: %5.2f%%" % (trainer.totalepochs,
	trnresult, tstresult)


out = nnet.activateOnDataset(tstdata)
out = out.argmax(axis=1)











#PCA
scaler = StandardScaler(copy = True, with_mean = True,
	with_std = True).fit(vectors)
vectors_scaled = scaler.transform(vectors)

pca = RandomizedPCA()
pca.fit(vectors_scaled)
print "Variance Explained: %f" % sum(pca.explained_variance_ratio_)
pca_vectors = pca.transform(vectors_scaled)




pca = PCA(n_components = 100)
pca.fit(vectors_scaled)
print "Variance Explained: %f" % sum(pca.explained_variance_ratio_)
pca_vectors = pca.transform(vectors_scaled)


#modeling

regularizer = [0.0001, 0.01, 0.03, 0.10, 0.33, 1.0, 3.0, 10.0, 20, 50, 100]
gamma = [0.0, 0.1, 0.3, 1.0, 3.0, 10.0]
best_accuracy = 0

for l in regularizer:

	# SVM = SGDClassifier(loss = 'hinge', penalty = 'l2', 
	# 	alpha = l,
	# 	n_jobs = -1, n_iter = 10, shuffle = True, 
	# 	warm_start = True)
	for g in gamma:
		SVM = SVC(C = l, gamma = g)
		scores = cross_validation.cross_val_score(estimator = SVM, X = pca_vectors, 
			y = labels,
			scoring = 'accuracy')
		accuracy = np.mean(scores)
		std_err = np.std(scores) * 1.96
		if accuracy  > best_accuracy:
			best_accuracy = accuracy
			best_parameters = (l,g,accuracy,std_err)
		print "Lambda: %f  Gamma: %f  Accuracy: %f  SD Err.: %f" % (l, g, accuracy, std_err)

print "Best Parameters:"
print "\tLambda: %f  Accuracy: %f  SD Err.: %f" % (best_parameters[0], best_parameters[1], best_parameters[2])


#fit final model to full training set
SVM = SGDClassifier(loss = 'hings', penalty = 'l2',
	alpha = best_parameters[0],
	j_jobs = -1, n_iter = 10, shuffle = True,
	warm_start = True):
SVM.fit(pca_vectors, labels)

#apply to test set
return_dir = os.getcwd()
os.chdir(DIRECTORY + TRAIN)

test = os.listdir(os.getcwd())

with open('predictions.csv','wb') as f:
	writer = csv.writer()
	writer.writerrow(['id','label'])
	for im in test:
		X = creat_vector(im, (100, 100) )
		identifier = int(im.split('.')[0])
		test_vectors_scaled = scaler.transform(test_vectors)
		pca_test_vectors = pca.transform(test_vectors_scaled)
		prediction = SVM.predict(pca_test_vectors)
		writer.writerow([identifier,prediction])

os.chdir(return_dir)



